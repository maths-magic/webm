WD=$PWD
# Build native libs

cd $WD/src/main/libogg
./configure
make

cd $WD/src/main/libvorbis
./configure --with-ogg=$PWD/../libogg/ --with-ogg-libraries=$PWD/../libogg/src/.libs/
make

cd $WD/src/main/libvpx
echo "you may need to \"yum install yasm\""
./configure --enable-pic --enable-shared
make

cd $WD/src/main/libwebm
make libwebm.so

# Copy native so libs to ./lib
cd $WD
mkdir -p lib
cp src/main/libogg/src/.libs/libogg.so lib/
cp src/main/libvorbis/lib/.libs/libvorbis.so lib/
cp src/main/libvpx/libvpx.so.1.1.0 lib/libvpx.so
cp src/main/libwebm/libwebm.so lib/

# Build JNI bindings
cd $WD/src/main
make


# Build jar file
cd $WD
mvn clean install

